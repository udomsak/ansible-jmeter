# ติดตั้ง

1. apt-get install ansible python-dev python-setuptools git build-essential libssl-dev libffi-dev 
2. apt-get remove ansible 
3. easy_install pip 
4. pip install ansible 
5. cd / && git clone https://udomsak@bitbucket.org/udomsak/ansible-jmeter.git
6. cd ansible-jmeter && ansible-galaxy install williamyeh.oracle-java
7. แก้ไข hosts ไฟล์ โดยเพิ่ม และ เปลี่ยนเบอร์ ip ของ jmeter-slave ให้เหมาะสม 

# ทดสอบ ansible config ถูก

- ansible jmeter-master -m setup 
- ansible jmeter-slave -m setup 



# ติดตั้ง Java + Jmeter 

ansible-playbook init.yml 


# Jmeter folder 

จะอยู่ที่ /Jmeter/apache-jmeter-3.1

เข้าไปที่ bin สั่งรัน ./jmeter  